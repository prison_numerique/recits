//* Track Source: PixaBay

const tracks = [
  {
    artwork:
      "./media/cover_recit-0.png",
    name: "Introduction - Récit 0",
    artist: "Lucie Alidières",
    path: "./media/recit_0.mp3",
  },
  {
    artwork:
      "./media/cover_recit-1.png",
    name: "Chapitre 2 - Récit 1",
    artist: "Lucie Alidières",
    path: "./media/recit_1.mp3",
  },
  {
    artwork:
    "./media/cover_recit-2.png",
    name: "Chapitre 2 - Récit 2",
    artist: "Lucie Alidières",
    path: "./media/recit_2.mp3",
  },
  {
    artwork:
      "./media/cover_recit-3.png",
    name: "Chapitre 4 - Récit 3",
    artist: "Lucie Alidières",
    path: "./media/recit_4.mp3",
  },
  {
    artwork:
      "./media/cover_recit-4.png",
    name: "Chapitre 5 - Récit 4",
    artist: "Lucie Alidières",
    path: "./media/recit_4.mp3",
  },
  {
    artwork:
      "./media/cover_recit-5.png",
    name: "Chapitre 6 - Récit 5",
    artist: "Lucie Alidières",
    path: "./media/recit_5.mp3",
  },
];
